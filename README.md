# BlissIconPack

Bliss Icon Pack is an open-source icon pack licensed and distributed under [The GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).  
It ships as the default icon pack on the /e/ OS. It offers rounded rectangular icons for the default applications and is used by the [BlissLauncher](https://gitlab.e.foundation/e/apps/BlissLauncher).

## Compatibility
Bliss Icon Pack is compatible from API 25 (Android Nougat | 7.1.0) to API 30 (Android R | 11.0)

## Development
- A the list of contributors can be viewed on this repository's [contributors graph](https://gitlab.e.foundation/e/apps/BlissIconPack/-/graphs/master).

In case you wish to contribute to the development of this project, feel free to open a [Merge Request](https://gitlab.e.foundation/e/apps/BlissIconPack/-/merge_requests) or an [Issue](https://gitlab.e.foundation/e/backlog/-/issues/) for the same. Contributions are always welcome.
